<?php

use App\Http\Controllers\PostsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [\App\Http\Controllers\FrontendController::class, 'index'])->name('home');
Route::get('/blogs/category/{category}', [\App\Http\Controllers\FrontendController::class, 'categories'])->name('category.blogs');
Route::get('/blogs/tag/{tag}', [\App\Http\Controllers\FrontendController::class, 'tags'])->name('tag.blogs');

/**
 * ADMIN PANEL
 */

Route::middleware(['auth'])->group(function() {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });
    Route::resource('posts', \App\Http\Controllers\PostsController::class);

    Route::get('/trashed/posts', [\App\Http\Controllers\PostsController::class,'trashed'])->name('posts.trashed');
    Route::put('/restored/posts/{post}', [\App\Http\Controllers\PostsController::class,'restored'])->withTrashed()->name('posts.restored');
    Route::delete('/trashDelete/posts/{post}', [\App\Http\Controllers\PostsController::class,'trashDeleted'])->withTrashed()->name('posts.trashDeleted');

    Route::get('/singlePost/{post}', [\App\Http\Controllers\PostsController::class, 'singlePost'])->name('posts.singlePost');
    Route::get('/singlePost/category/{category}', [\App\Http\Controllers\PostsController::class, 'categories'])->name('category.singlePost');
    Route::get('/singlePost/tag/{tag}', [\App\Http\Controllers\PostsController::class, 'tags'])->name('tag.singlePost');

    Route::resource('categories', \App\Http\Controllers\CategoriesController::class);
    Route::resource('tags', \App\Http\Controllers\TagsController::class)->except('show');
});

Route::middleware(['auth', 'admin'])->group(function() {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.makeAdmin');
    Route::put('/users/{user}/make-author', [UsersController::class, 'makeAuthor'])->name('users.makeAuthor');
});

require __DIR__.'/auth.php';
