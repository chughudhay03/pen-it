@extends('frontend.layout.app')

@section('page-content')
<div class="row">
    @forelse ($posts as $post)
        <div class="col-md-4 col-sm-6 col-xs-12 mb50">
            <h4 class="blog-title"><a href="#">{{ $post->title }}</a></h4>
            <div class="blog-three-attrib">
                <span class="icon-calendar"></span>{{ $post->published_at->diffForHumans() }}
                <span class=" icon-pencil"></span><a href="#">{{ $post->author->name }}</a>
            </div>
            <img src="{{ asset("/storage/$post->image") }}" class="img-responsive" alt="image blog">
            <p class="mt25">{{ $post->excerpt }}</p>
            <a href="{{ route('posts.singlePost', $post) }}" class="button button-gray button-xs">Read More <i class="fa fa-long-arrow-right"></i></a>
        </div>
    @empty
        <h2>No Post Found</h2>

    @endforelse
</div>

<!-- Blog Paging
===================================== -->
<div class="row mt25 animated" data-animation="fadeInUp" data-animation-delay="100">
    {{ $posts->appends(['search'=>request('search')])->links('frontend.layout.partials._pagination') }}
</div>

@endsection

@section('scripts')
    <script>
        const button = document.querySelector('.btnDisable')
        button.style.pointerEvents="none";
        button.style.opacity = "0.4";
    </script>
@endsection
