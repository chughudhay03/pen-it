@if ($paginator->hasPages())
    <div class="col-md-6">
        @if ($paginator->onFirstPage())
            <a href="#" class="button button-sm button-pasific pull-left hover-skew-backward btnDisable">
                Old Entries
            </a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="button button-sm button-pasific pull-left hover-skew-backward">
                Old Entries
            </a>
        @endif
    </div>
    <div class="col-md-6">
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="button button-sm button-pasific pull-right hover-skew-forward">
                New Entries
            </a>
        @else
            <a href="#" class="button button-sm button-pasific pull-right hover-skew-forward btnDisable">
                New Entries
            </a>
        @endif
    </div>
@endif
