@extends('frontend.singlepost.layout.app')

@section('page-content')
<div class="blog-three-mini">
    <h2 class="color-dark"><a href="#">{{ $post->title }}</a></h2>
    <div class="blog-three-attrib">
        <div><i class="fa fa-calendar"></i>{{ $post->published_at->diffForHumans() }}</div> |
        <div><i class="fa fa-pencil"></i><a href="#">{{ $post->author->name }}</a></div> |
        <div><i class="fa fa-comment-o"></i><a href="#">90 Comments</a></div> |
        <div><a href="#"><i class="fa fa-thumbs-o-up"></i></a>150 Likes</div> |
        <div>
            Share:  <a href="#"><i class="fa fa-facebook-official"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
        </div>
    </div>

    <img src="{{ asset("/storage/$post->image") }}" alt="Blog Image" class="img-responsive">
    <p class="lead mt25">
        {{ $post->body }}
    </p>
    <blockquote>
        {{ $post->excerpt }}
        <footer><i class="fa fa-quote-right mt25"></i> {{ $post->author->name }}</footer>
    </blockquote>
    <div class="blog-post-read-tag mt50">
        <i class="fa fa-tags"></i> Tags:
        @foreach($post->tags as $tag)
           <a class="tags" href="">{{ $tag->name }}</a>,
        @endforeach
    </div>
</div>
@endsection
