@if ($paginator->hasPages())
    <div class="col-md-12">
        @if ($paginator->onFirstPage())
            <a href="#" class="btn btn-primary float-left hover-skew-backward btnDisable">
                Old Entries
            </a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="btn btn-primary float-left hover-skew-backward">
                Old Entries
            </a>
        @endif
    </div>
    <div class="col-md-12">
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="btn btn-primary float-right hover-skew-forward">
                New Entries
            </a>
        @else
            <a href="#" class="btn btn-primary float-right hover-skew-forward btnDisable">
                New Entries
            </a>
        @endif
    </div>
@endif
