@extends('admin.layouts.app')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="float-right mb-3">
                <a href="{{ route('posts.create') }}" class="btn btn-primary">Add New Post</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Posts</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <th>Id</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Excerpt</th>
                            <th>Category</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>
                                        <img src="{{ asset("storage/$post->image") }}" class="img-responsive" width="100px">
                                    </td>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->excerpt }}</td>
                                    <td>{{ $post->category->name }}</td>
                                    <td>
                                        @if ($post->is_owner)
                                            <a href="{{ route('posts.edit', $post) }}" class="btn btn-info btn-sm">Edit</a>
                                            <button class="btn btn-danger btn-sm trash" data-bs-toggle="modal" data-bs-target="#trashModal" data-post-id="{{ $post->id }}">Trash Bin</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div>
            {{ $posts->links('admin.layouts.partials._pagination') }}
        </div>
    </div>

    <!-- Delete Confirmation Modal -->
    <div class="modal fade" id="trashModal" tabindex="-1" aria-labelledby="trashModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="trashModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Are You Sure, You Want to Move in Trash?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <form id="trashForm" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Yes, Move To Trash</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        document.querySelectorAll('.trash').forEach(function (btn) {
            btn.addEventListener('click', handleTrashClick);
        });

        function handleTrashClick(evt) {
            const postId = evt.target.dataset.postId;
            const URL = `/posts/${postId}`;
            document.getElementById('trashForm').setAttribute('action', URL);
        }

        const button = document.querySelector('.btnDisable')
        button.style.pointerEvents="none";
        button.style.opacity = "0.4";
    </script>
@endsection
