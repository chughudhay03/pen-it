<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'category_id',
        'user_id',
        'title',
        'excerpt',
        'body',
        'image',
        'published_at'
    ];

    protected $dates = ['published_at'];

    /** RELATIONSHIP METHODS */
    public function tags() {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * HELPER METHODS
     */
    public function hasTag($tagId) {
        return in_array($tagId, $this->tags()->get()->pluck('id')->toArray());
    }

    public function deleteImage() {
        Storage::delete($this->image);
    }

    public function getPublishedDateStringAttribute()
    {
        return Carbon::createFromFormat('Y-m-d h:i:s', $this->published_at)
            ->format('Y-m-d');
    }

    public function getIsOwnerAttribute()
    {
        return $this->user_id === auth()->id();
    }

    /**
     * Scopes
     */
    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now());
    }

    public function scopeSearch($query)
    {
        $search = request('search');
        if($search)
        {
            return $query->where('title', 'like', "%$search%");
        }
        return $query;
    }
}
