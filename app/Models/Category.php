<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    /**
     * @return int
     * Accessor to return post count related to category
     */
    public function getPostsCountAttribute()
    {
        return $this->posts()->count();
    }
    
    /** RELATIONSHIP METHODS */
    public function posts() {
        return $this->hasMany(Post::class);
    }
}
