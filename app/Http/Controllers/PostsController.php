<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['verifyCategoryCount'])->only(['create']);
        $this->middleware(['verifyTagCount'])->only(['create']);
        $this->middleware(['validatePostAuthor'])->only(['edit', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('category')->simplePaginate(4);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.create', compact(['tags', 'categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image')->store('posts');
        // Remember to run php artisan storage:link
        // and also in .env make FILESYSTEM_DISK as public

        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'body' => $request->body,
            'published_at' => $request->published_at,
            'category_id' => $request->category_id,
            'user_id' => auth()->id(),
            'image' => $image,
        ]);

        // Used to enter in many to many table
        $post->tags()->attach($request->tags);

        return redirect(route('posts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Post Added Successfully!'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.edit', compact([
            'tags',
            'categories',
            'post'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $data = $request->only(['title', 'excerpt', 'body', 'published_at', 'category_id']);

        if($request->hasFile('image')) {
            $image = $request->file('image')->store('posts');
            $data['image'] = $image;
            $post->deleteImage();
        }
        $post->update($data);

        $post->tags()->sync($request->tags);

        return redirect(route('posts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Post Updated Successfully!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect(route('posts.index'))->with(
            [
                'status' => 'success',
                'message' => 'Post Move To Trash Successfully!'
            ]
        );
    }

    public function trashed()
    {
        $posts = Post::onlyTrashed()->get();
        return view('admin.posts.trashed', compact('posts'));
    }

    public function restored(Post $post) {
        $post->restore();
        return redirect(route('posts.index'))->with(
            [
                'status' => 'success',
                'message' => 'Post Remove From Trash Successfully!'
            ]
        );
    }

    public function trashDeleted(Post $post)
    {
        // dd($post);
        $post->deleteImage();
        $post->forceDelete();
        return redirect(route('posts.trashed'))->with(
            [
                'status' => 'success',
                'message' => 'Post Deleted Successfully!'
            ]
        );
    }

    public function singlePost($postId)
    {
        $post = Post::findOrFail($postId);
        $categories = Category::all();
        $tags = Tag::all();
        return view('frontend.singlepost.index', compact(['post', 'categories', 'tags']));
    }

    public function categories(Category $category) {
        $posts = $category->posts()->published();
        $categories = Category::all();
        $tags = Tag::all();
        return view('frontend.singlepost.index', compact(['categories', 'tags', 'posts']));
    }

    public function tags(Tag $tag) {
        $posts = $tag->posts()->published();
        $categories = Category::all();
        $tags = Tag::all();
        return view('frontend.singlepost.index', compact(['categories', 'tags', 'posts']));
    }
}
