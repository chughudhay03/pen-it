<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::simplePaginate(4);
        return view('admin.tags.index', compact(['tags']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tag::create(['name' => $request->name]);
        session()->flash('status', 'success');
        session()->flash('message', 'Tag Added Successfully!');
        return redirect(route('tags.index'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('admin.tags.edit', compact(['tag']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $tag->update(['name' => $request->name]);
        session()->flash('status', 'success');
        session()->flash('message', 'Tag Updated Successfully!');
        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if($tag->posts()->count() > 0) {
            session()->flash('status', 'danger');
            session()->flash('message', 'Tag has Posts! Cannot Delete');
            return redirect(route('tags.index'));
        }

        $tag->delete();
        session()->flash('status', 'success');
        session()->flash('message', 'Tag Deleted Successfully!');
        return redirect(route('tags.index'));

    }
}
