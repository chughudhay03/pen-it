<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::simplePaginate(4);
        return view('admin.categories.index', compact(['categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            Category::create(['name' => $request->name]);
            return redirect(route('categories.index'))
                ->with(['status' => 'success',
                    'message' => 'Category Added Successfully!'
                ]);
        } catch(\Exception $e) {
            return redirect(route('categories.index'))
                ->with(['status' => 'danger',
                    'message' => 'OOPS! Something Went Wrong!'
                ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact(['category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        // dd($request->name);
        // $category->name = $request->name;
        // $category->save();
        $category->update(['name' => $request->name]);
        return redirect(route('categories.index'))->with(
            [
                'status' => 'success',
                'message' => 'Category Updated Successfully!'
            ]
        );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // We need to delete only those categories which does not have any posts associated!
        if($category->posts()->count() > 0) {
            return redirect(route('categories.index'))->with(
                [
                    'status' => 'danger',
                    'message' => 'Category Has Posts! Cannot Delete'
                ]
            );
        }
        $category->delete();
        return redirect(route('categories.index'))->with(
            [
                'status' => 'success',
                'message' => 'Category Deleted Successfully!'
            ]
        );

    }

}
