<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Tag;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name'=>'News']);
        $categoryDesign = Category::create(['name'=>'Design']);
        $categoryTechnology = Category::create(['name'=>'Technology']);
        $categoryEngineering = Category::create(['name'=>'Engineering']);

        $tagCustomer = Tag::create(['name'=>'Customer']);
        $tagFrontEnd = Tag::create(['name'=>'FrontEnd']);
        $tagBackend = Tag::create(['name'=>'Backend']);
        $tagCoding = Tag::create(['name'=>'Coding']);
        $tagJava = Tag::create(['name'=>'Java']);

        $post1 = Post::create([
            'category_id' => $categoryDesign->id,
            'user_id' => User::all()->random()->id,
            'title' => Factory::create()->title(),
            'excerpt' => fake()->sentence(),
            'body' => fake()->paragraph(10),
            'image'=>'posts/1.jpg',
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'category_id' => $categoryEngineering->id,
            'user_id' => User::all()->random()->id,
            'title' => Factory::create()->title(),
            'excerpt' => fake()->sentence(),
            'body' => fake()->paragraph(10),
            'image'=>'posts/2.jpg',
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post3 = Post::create([
            'category_id' => $categoryNews->id,
            'user_id' => User::all()->random()->id,
            'title' => Factory::create()->title(),
            'excerpt' => fake()->sentence(),
            'body' => fake()->paragraph(10),
            'image'=>'posts/3.jpg',
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post4 = Post::create([
            'category_id' => $categoryTechnology->id,
            'user_id' => User::all()->random()->id,
            'title' => Factory::create()->title(),
            'excerpt' => fake()->sentence(),
            'body' => fake()->paragraph(10),
            'image'=>'posts/4.jpg',
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post5 = Post::create([
            'category_id' => $categoryNews->id,
            'user_id' => User::all()->random()->id,
            'title' => Factory::create()->title(),
            'excerpt' => fake()->sentence(),
            'body' => fake()->paragraph(10),
            'image'=>'posts/5.jpg',
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post6 = Post::create([
            'category_id' => $categoryEngineering->id,
            'user_id' => User::all()->random()->id,
            'title' => Factory::create()->title(),
            'excerpt' => fake()->sentence(),
            'body' => fake()->paragraph(10),
            'image'=>'posts/6.jpg',
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagCustomer->id, $tagFrontEnd->id]);
        $post2->tags()->attach([$tagCoding->id, $tagBackend->id]);
        $post3->tags()->attach([$tagCustomer->id, $tagFrontEnd->id, $tagBackend->id]);
        $post4->tags()->attach([$tagCustomer->id, $tagCoding->id, $tagJava->id]);
        $post5->tags()->attach([$tagCustomer->id, $tagFrontEnd->id, $tagJava->id]);
        $post6->tags()->attach([$tagBackend->id, $tagCoding->id, $tagJava->id]);
    }
}
